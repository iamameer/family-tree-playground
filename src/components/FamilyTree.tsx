//! V1
// import React from 'react';
// import { Group } from '@visx/group';
// import { Tree, hierarchy } from '@visx/hierarchy';
// import { LinkHorizontal } from '@visx/shape';
// import { LinearGradient } from '@visx/gradient';

// interface TreeNode {
//   name: string;
//   children?: TreeNode[];
// }

// const treeData: TreeNode = {
//   name: 'Father',
//   children: [
//     {
//       name: 'Me',
//       children: [
//         { name: 'My wife' },
//         { name: 'My brother' },
//         { name: 'My son' },
//       ],
//     },
//     { name: 'Mother' },
//   ],
// };

// const FamilyTree: React.FC = () => {
//   const data = hierarchy(treeData);

//   return (
//     <svg width={1000} height={1000}>
//       <LinearGradient id="link-gradient" from="#fd9b93" to="#fe6e9e" />
//       <rect width={1000} height={1000} rx={14} fill="#272b4d" />
//       <Tree<TreeNode> root={data} size={[400, 400]}>
//         {(tree) => (
//           <Group top={40} left={40}>
//             {tree.links().map((link, i) => (
//               <LinkHorizontal
//                 key={`link-${i}`}
//                 data={link}
//                 stroke="url('#link-gradient')"
//                 strokeWidth="1"
//                 fill="none"
//               />
//             ))}
//             {tree.descendants().map((node, i) => (
//               <Group key={`node-${i}`} top={node.y} left={node.x}>
//                 <circle r={12} fill="#ffffff" stroke="#03c0dc" strokeWidth={3} />
//                 <text
//                   dy=".33em"
//                   fontSize={9}
//                   fontFamily="Arial"
//                   textAnchor="middle"
//                   style={{ pointerEvents: 'none' }}
//                   fill="#ffffff"
//                 >
//                   {node.data.name}
//                 </text>
//               </Group>
//             ))}
//           </Group>
//         )}
//       </Tree>
//     </svg>
//   );
// };

// export default FamilyTree;


//! V2
// import React from 'react';
// import { Tree } from 'react-d3-tree';

// const myFamily = {
//   name: 'Me',
//   children: [
//     {
//       name: 'Father',
//       children: [
//         {
//           name: 'Brother',
//         },
//       ],
//     },
//     {
//       name: 'Mother',
//     },
//     {
//       name: 'My wife',
//     },
//     {
//       name: 'My son',
//     },
//   ],
// };

// const FamilyTree: React.FC = () => {
//   return (
//     <div style={{ width: '100%', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
//       <Tree data={myFamily} orientation="vertical" />
//     </div>
//   );
// };

// export default FamilyTree;

//! V3
import React from 'react';

const FamilyTree: React.FC = () => {
  return (
    <div className="flex justify-center items-center">
      <table className="border-collapse border border-black">
        <tbody>
          <tr>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
            <td className="border border-black w-16 h-16 text-center bg-green-200">Father</td>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
            <td className="border border-black w-16 h-16 text-center bg-pink-200">Mother</td>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
          </tr>
          <tr>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
            <td className="border border-black w-16 h-16 text-center bg-pink-200">My wife</td>
            <td className="border border-black w-16 h-16 text-center bg-blue-200">Me</td>
            <td className="border border-black w-16 h-16 text-center bg-yellow-200">Brother</td>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
          </tr>
          <tr>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
            <td className="border border-black w-16 h-16 text-center bg-yellow-200">My son</td>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
            <td className="border border-black w-16 h-16 text-center bg-blue-200"> </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default FamilyTree;


import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import Dendogram from './components/Dendogram';
import Trees from "./components/Trees";
import FamilyTree from './components/FamilyTree';
// import ParentSize from '@visx/responsive/lib/components/ParentSize';

// import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
    {/* <ParentSize>{({ width, height }) =>  */}

    {/* <Dendogram width={800} height={600} /> */}
    {/* <Trees width={1000} height={1000} /> */}
    <FamilyTree />

    {/* }</ParentSize> */}

      {/* <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p> */}
    </>
  )
}

export default App
